# `geopic-tag-reader`

Reads EXIF metadata from a picture file, and prints results

**Usage**:

```console
$ geopic-tag-reader [OPTIONS]
```

**Options**:

* `--image PATH`: Path to your JPEG image file  [required]
* `--install-completion`: Install completion for the current shell.
* `--show-completion`: Show completion for the current shell, to copy it or customize the installation.
* `--help`: Show this message and exit.
